/**
 *
 * HomePage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { compose } from 'redux';
import axios from 'axios';
import House from 'components/Houses';

/* eslint-disable react/prefer-stateless-function */
export class HomePage extends React.PureComponent {
  componentDidMount() {
    axios
      .get('https://game-of-throne-api.appspot.com/api/houses')
      .then(response => {
        this.setState({ houses: response.data });
      })
      .catch(error => {
        // eslint-disable-next-line
        console.error(error);
      });
  }

  state = {
    houses: false,
  };

  render() {
    return (
      <div>
        <Helmet>
          <title>HomePage</title>
          <meta name="description" content="Description of HomePage" />
        </Helmet>
        {this.state.houses.length > 0
          ? this.state.houses.map((h) => <House key={h.key} house={h} />)
          : null}
      </div>
    );
  }
}

HomePage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  null,
  mapDispatchToProps,
);

export default compose(withConnect)(HomePage);
