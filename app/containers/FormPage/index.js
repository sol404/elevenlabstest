/**
 *
 * FormPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

/* eslint-disable react/prefer-stateless-function */
export class FormPage extends React.PureComponent {
  state = {
    name: '',
  };

  handleSubmitForm = e => {
    e.preventDefault();
    if (this.state.name.length > 0) alert(this.state.name);
  };

  handleInputForm = e => this.setState({ name: e.target.value.trim() });

  render() {
    return (
      <form onSubmit={this.handleSubmitForm}>
        <p>
          <label htmlFor="name">Votre nom</label>
          <br />
          <input
            style={{ border: '1px solid black', padding: 5 }}
            type="text"
            id="name"
            name="name"
            onInput={this.handleInputForm}
            placeholder="Votre nom"
          />
        </p>

        <button type="submit">Afficher</button>
      </form>
    );
  }
}

FormPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  null,
  mapDispatchToProps,
);

export default compose(withConnect)(FormPage);
