/**
 *
 * Asynchronously loads the component for FormPage
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
