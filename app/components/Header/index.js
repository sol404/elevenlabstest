/**
 *
 * Header
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import H1 from 'components/H1';
import Navigation from 'components/Navigation';
import { Container, Row, Col } from 'react-grid-system';

/* eslint-disable react/prefer-stateless-function */
class Header extends React.Component {
  render() {
    return (
      <Container>
        <Row>
          <Col sm={3}>
            <H1>header</H1>
          </Col>
          <Col sm={9}>
            <Navigation />
          </Col>
        </Row>
      </Container>
    );
  }
}

Header.propTypes = {};

export default Header;
