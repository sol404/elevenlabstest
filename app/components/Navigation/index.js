/**
 *
 * Navigation
 *
 */

import React from 'react';
import { Link } from 'react-router-dom';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

/* eslint-disable react/prefer-stateless-function */
class Navigation extends React.Component {
  render() {
    return (
      <ul>
        <li>
          <Link to="/">Accueil</Link>
        </li>
        <li>
          <Link to="/formulaire">Formulaire </Link>
        </li>
      </ul>
    );
  }
}

Navigation.propTypes = {};

export default Navigation;
