/**
 *
 * Houses
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import axios from 'axios';
import Img from 'components/Img';
import Button from 'components/Button';
import { Container, Row, Col } from 'react-grid-system';

/* eslint-disable react/prefer-stateless-function */
class Houses extends React.Component {
  state = {
    name: null,
  };

  handleCLick = () => {
    if (this.state.name === null) {
      axios
        .get(
          'https://game-of-throne-api.appspot.com/api/character/L2luZGV4LnBocC9Ub21tZW5fQmFyYXRoZW9u',
        )
        .then(response => {
          this.setState({ name: response.data.name });
        })
        .catch(error => {
          // eslint-disable-next-line
          console.error(error);
        });
    }
  };

  render() {
    const { house } = this.props;
    return (
      <div>
        <Container style={{ margin: 10, backgroundColor: 'whitesmoke' }}>
          <Row>
            <Col sm={12}>
              <Img
                alt={house.name}
                src={
                  house.imageUrl !== null
                    ? house.imageUrl
                    : 'https://i2.wp.com/www.heyuguys.com/images/2012/11/Game-Of-Thrones-180x150.jpg?resize=180%2C150'
                }
              />
            </Col>
            <Col sm={9}>
              <ul>
                <li>Nom: {house.name}</li>
                <li>Key: {house.key}</li>
                <li>Fondée: {house.founded}</li>
                <li>Fondateur: {house.founder}</li>
                <li>Région: {house.region}</li>
                {house.seats !== null ? (
                  <li>
                    Places:
                    <ul>
                      {house.seats.map(s => (
                        <li key={s}>{s}</li>
                      ))}
                    </ul>
                  </li>
                ) : null}
                {house.heirsKey !== null ? (
                  <li>
                    Héritiers clé: :
                    <ul>
                      {house.heirsKey.map(h => (
                        <li key={h}>{h}</li>
                      ))}
                    </ul>
                  </li>
                ) : null}
              </ul>
            </Col>
            {house.lordKey !== null ? (
              <Col sm={4}>
                {
                  <Button onClick={() => this.handleCLick(house.lordKey)}>
                    {this.state.name
                      ? this.state.name
                      : 'Qui est le Lord Commandant'}
                  </Button>
                }
              </Col>
            ) : null}
          </Row>
        </Container>
      </div>
    );
  }
}

Houses.propTypes = {
  house: PropTypes.object.isRequired,
};

export default Houses;
